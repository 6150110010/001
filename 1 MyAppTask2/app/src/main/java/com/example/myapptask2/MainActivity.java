package com.example.myapptask2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button bt1 = findViewById(R.id.button1);
        Button bt2 = findViewById(R.id.button2);
        Button bt3 = findViewById(R.id.button3);
        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
        bt3.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        TextView tv = (TextView) findViewById(R.id.textView1);
        switch(v.getId()) {
            case R.id.button1:
                // Do something
                tv.setText("You pressed 1st button.");
                break;
            case R.id.button2:
                // Do something
                tv.setText("You pressed 2nd button.");
                break;
            case R.id.button3:
                // Do something
                tv.setText("You pressed 3rd button.");
                break;
        }
    }
}
